<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/register', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'register.html', $args);
});

$app->get('/login', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'login.html', $args);
});

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.html', $args);
});
